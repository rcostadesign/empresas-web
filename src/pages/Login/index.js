/* eslint-disable no-console */
import React, { useState } from 'react';
import { Button, Container } from 'react-materialize';

import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';

import api from '../../services/api';
import logo from '../../assets/logo-home.png';
import { login } from '../../services/auth';

import * as S from './styled';

import Loading from '../../components/Loading';

function Login() {
  const [errorOnSubmit, setErrorOnSubmit] = useState(false);
  const [loading, setLoading] = useState(false);

  const history = useHistory();
  const { register, handleSubmit, errors } = useForm();

  const onSubmit = async (data) => {
    setLoading(true);

    const { email, password } = data;

    await api
      .post('/users/auth/sign_in', {
        email,
        password,
      })
      .then((response) => {

        const { headers } = response;

        login(headers['access-token'], headers.client, headers.uid);

        history.push('/');
        setLoading(false);
      })
      .catch(() => {
        setErrorOnSubmit(true);
        setLoading(false);
      });
  };

  return (
    <S.LoginWrapper>
      <S.LoginContainer>
        <Container>
          <img src={logo} alt="Empresas Web" />
          <S.LoginTitle>Bem-vindo ao Empresas</S.LoginTitle>
          <S.LoginDescription>
            Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.
          </S.LoginDescription>

          <S.LoginForm onSubmit={handleSubmit(onSubmit)}>
            <div className="input-field col s12">
              <i className="material-icons prefix">mail_outline</i>
              <input
                id="email"
                name="email"
                type="email"
                className={`validate ${errorOnSubmit && 'error'}`}
                placeholder="E-mail"
                ref={register({
                  required: 'Digite seu e-mail',
                  pattern: {
                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                    message: 'Por favor digite um e-mail válido!',
                  },
                })}
              />
              {errors.email && (
                <span
                  className="helper-text"
                  data-error="Por favor digite um e-mail válido!"
                  data-success="right"
                >
                  {errors.email.message}
                </span>
              )}
            </div>

            <div className="input-field col s12">
              <i className="material-icons prefix">lock_open</i>
              <input
                id="password"
                name="password"
                type="password"
                className="validate"
                placeholder="Senha"
                ref={register({
                  required: 'Digite sua senha',
                })}
              />
              {errors.password && (
                <span
                  className="helper-text"
                  data-error="Por favor digite um e-mail válido!"
                  data-success="right"
                >
                  {errors.password.message}
                </span>
              )}
              {errorOnSubmit && (
                <span
                  className="helper-text helper-center"
                  data-error="Credenciais informadas são inválidas, tente novamente."
                >
                  Credenciais informadas são inválidas, tente novamente.
                </span>
              )}
            </div>

            <Button
              large
              node="button"
              style={{
                marginRight: '5px',
                width: '100%',
              }}
              waves="light"
              type="submit"
            >
              Entrar
            </Button>
          </S.LoginForm>
        </Container>
      </S.LoginContainer>
      {loading && <Loading />}
    </S.LoginWrapper>
  );
}

export default Login;
