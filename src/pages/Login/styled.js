import styled from 'styled-components';

export const LoginWrapper = styled.section`
  min-height: 100vh;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const LoginContainer = styled.main`
  max-width: 520px;
  text-align: center;
`;

export const LoginTitle = styled.h1`
  color: #383743;
  font-size: 22px;
  font-weight: bold;
  line-height: 1.4;
  margin-top: 50px;
  padding-right: 60px;
  padding-left: 60px;
  text-transform: uppercase;
`;

export const LoginDescription = styled.p`
  color: #383743;
  font-size: 18px;
`;

export const LoginForm = styled.form`
  .input-field .prefix {
    color: #ee4c77 !important;
    font-size: 24px;
    margin-top: 3px;
  }

  label {
    font-size: 20px;
  }

  .helper-text {
    margin-left: 10px!important;
    text-align: left!important;
  }

  .helper-center {
    text-align: center!important;
  }

  input {
    color: #403e4d;
    font-size: 20px!important;
    margin-left: 0 !important;
    padding-left: 3rem !important;

    &::-webkit-input-placeholder {
      font-size: 20px;
      color: #383743;
      opacity: 50%;
    }

    &:-moz-placeholder {
      /* Firefox 18- */
      font-size: 20px;
      color: #383743;
      opacity: 50%;
    }

    &::-moz-placeholder {
      /* Firefox 19+ */
      font-size: 20px;
      color: #383743;
      opacity: 50%;
    }

    &:-ms-input-placeholder {
      font-size: 20px;
      color: #383743;
      opacity: 50%;
    }
  }

  button {
    background: #57bbbc;
    font-weight: bold;
  }
`;
