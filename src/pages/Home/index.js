import React, { useState, useEffect } from 'react';

import {
  Row,
  Col,
  NavItem,
  Icon,
  Container,
  Button,
} from 'react-materialize';
import { Link, useHistory } from 'react-router-dom';
import { logout } from '../../services/auth';

import api from '../../services/api';

import Loading from '../../components/Loading';

import logo from '../../assets/logo-nav.png';
import * as S from './styled';

function Home() {
  const [companies, setCompanies] = useState();
  const [searchOpen, setSearchOpen] = useState(false);
  const [searchCurrentText, setSearchCurrentText] = useState('');
  const [loading, setLoading] = useState(false);

  const history = useHistory();

  const getCompanies = async () => {
    setLoading(true);
    await api
      .get('/enterprises')
      .then((response) => {
        const fetchCompanies = response.data.enterprises;
        setCompanies(fetchCompanies);
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    getCompanies();
  }, []);

  const handleClickLogOut = () => {
    logout();
    history.push('/login');
  };

  const handleSearchOpen = () => {
    setSearchOpen(!searchOpen);
  };

  const handleChangeSearch = (event) => {
  };

  return (
    <div>
      <S.HomeNavbar
        alignLinks="right"
        brand={
          (
            <Link className="brand-logo" to="/">
              <img src={logo} alt="Empresas web" />
            </Link>
          )
        }
        centerLogo
        id="mobile-nav"
        menuIcon={<Icon>menu</Icon>}
        options={{
          draggable: true,
          edge: 'right',
          inDuration: 250,
          onCloseEnd: null,
          onCloseStart: null,
          onOpenEnd: null,
          onOpenStart: null,
          outDuration: 200,
          preventScrolling: true,
        }}
      >
        <NavItem onClick={handleSearchOpen}>
          <Icon className="icon-search">search</Icon>
        </NavItem>

        {searchOpen && (
          <S.SearchWrapper>
            <S.SearchContent>
              <S.SearchIconSearch>
                <Icon>search</Icon>
              </S.SearchIconSearch>
              <S.SearchInput
                type="text"
                name="search"
                id="search"
                onChange={handleChangeSearch}
              />
              <S.SearchIconClose onClick={handleSearchOpen}>
                <Icon>close</Icon>
              </S.SearchIconClose>
            </S.SearchContent>
          </S.SearchWrapper>
        )}
      </S.HomeNavbar>

      <Container>
        <Row>
          <Col m={12} s={12}>
            <S.HomeMsg>
              <p>{searchCurrentText}</p>
            </S.HomeMsg>
            {companies &&
              companies.map((company) => (
                <S.CardList>
                  <div className="destaque">
                    <span>E {company.id}</span>
                  </div>
                  <div className="caption">
                    <h2>Empresa1</h2>
                    <h3>Negócio</h3>
                    <p>Brasil</p>
                  </div>
                </S.CardList>
              ))}
          </Col>
        </Row>
      </Container>
      {loading && <Loading />}
      <Button
        onClick={handleClickLogOut}
        className="red"
        fab={{
          direction: 'left',
          hoverEnabled: true,
        }}
        floating
        large
        node="button"
        icon={<Icon>exit_to_app</Icon>}
      >
        <Button
          className="red"
          floating
          icon={<Icon>exit_to_app</Icon>}
          node="button"
        />
      </Button>
    </div>
  );
}

export default Home;
