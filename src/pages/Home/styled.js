import styled from 'styled-components';

import { Navbar } from 'react-materialize';

export const HomeNavbar = styled(Navbar)`
  background: #ee4c77;
  background: -moz-linear-gradient(top, #ee4c77 0%, #c13e69 100%);
  background: -webkit-linear-gradient(top, #ee4c77 0%, #c13e69 100%);
  background: linear-gradient(to bottom, #ee4c77 0%, #c13e69 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ee4c77', endColorstr='#c13e69',GradientType=0 ); /* IE6-9 */
  height: 103px;

  .brand-logo.center {
    padding: 24px;
    display: flex;
  }

  ul {
    height: 100%;
    display: flex;
    align-items: center;
  }

  .icon-search {
    font-size: 36px;
  }
`;

export const HomeMsg = styled.div`
  /* -webkit-box-align: center; */
  align-items: center;
  justify-content: center;
  display: flex;
  flex-direction: column;
  height: 90vh;
  right: 0;
  left: 0;
  position: fixed;
  text-align: center;
  /* padding: 2rem; */

  p {
    font-size: 24px;
  }
`;

export const SearchWrapper = styled.div`
  background: #ee4c77;
  background: -moz-linear-gradient(top, #ee4c77 0%, #c13e69 100%);
  background: -webkit-linear-gradient(top, #ee4c77 0%, #c13e69 100%);
  background: linear-gradient(to bottom, #ee4c77 0%, #c13e69 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ee4c77', endColorstr='#c13e69',GradientType=0 ); /* IE6-9 */
  position: absolute;
  z-index: 999;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  padding: 16px 36px;
  display: flex;
  align-items: flex-end;
`;

export const SearchContent = styled.div`
  position: relative;
  /* background: yellow; */
  display: flex;
  flex-grow: 1;
  box-sizing: content-box;

  .material-icons {
    font-size: 36px;
    bottom: 7px;
  }
`;

export const SearchIconSearch = styled.div`
  left: 0;
  position: absolute;
`;

export const SearchIconClose = styled.div`
  right: 0;
  position: absolute;
  cursor: pointer;
`;

export const SearchInput = styled.input.attrs({
  type: 'text',
})`
  color: #fff !important;
  border-bottom: 1px solid #fff;
  padding-left: 2.8rem !important;
  padding-bottom: 10px !important;
  font-size: 36px !important;
`;

export const CardList = styled.div`
  background: #fff;
  display: flex;
  padding: 2rem;
  margin-bottom: 16px;
  margin-top: 24px;
  cursor: pointer;

  .destaque {
    flex: 1;
    min-height: 200px;
    background: #7dc075;
    margin-right: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #fff;
    font-weight: bold;
    font-size: 5rem;
    letter-spacing: -2;

    span {
      letter-spacing: -11px;
    }
  }

  .caption {
    flex: 2;

    h2 {
      color: #1a0e49;
      font-weight: bold;
      font-size: 36px;
      margin: 30px 0 10px;
    }
    h3 {
      color: #8d8c8c;
      font-size: 24px;
      margin: 10px 0 10px;
    }
    p {
      color: #8d8c8c;
      font-size: 16px;
    }
  }
`;
