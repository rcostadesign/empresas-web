import React from 'react';

import { Row, Col } from 'react-materialize';

const Page404 = () => (
  <div>
    <Row>
      <Col m={12} s={12}>
        <h1>404</h1>
        <h2>Não encontrado!</h2>
      </Col>
    </Row>
  </div>
);

export default Page404;
