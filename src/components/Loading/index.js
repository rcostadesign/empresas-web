import React from 'react';

import { Preloader } from 'react-materialize';

import * as S from './styled';

const Loading = () => (
  <S.LoadingWrapper>
    <Preloader active color="blue" flashing={false} size="big" />
  </S.LoadingWrapper>
);

export default Loading;
