import styled from 'styled-components';

export const LoadingWrapper = styled.div`
  background: rgba(255, 255, 255, 0.6);
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  z-index: 10;
`;
