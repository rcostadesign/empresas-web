import React from 'react';
import 'materialize-css/dist/css/materialize.min.css';
import './styles.css';

import Routes from './routes';

const App = () => <Routes />;

export default App;
