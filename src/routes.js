import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';

import PrivateRoute from './components/PrivateRoute';

import Login from './pages/Login';
import Home from './pages/Home';
import Page404 from './pages/Page404';

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route component={Login} path="/login" exact />
      <PrivateRoute component={Home} path="/" exact />
      <Route path="*" component={Page404} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
